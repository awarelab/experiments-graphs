FROM python:3.6

COPY requirements.txt .

RUN pip install -r requirements.txt

RUN curl https://registry.npmjs.org/mermaid/-/mermaid-8.5.0.tgz >mermaid.tgz;tar xf mermaid.tgz;cp package/dist/mermaid*js .;rm -rf package;rm mermaid.tgz