## 1. Run graph generation on hermes database: 

``python experiments_graph/main_app.py /tmp/graph.html --database ~/.hermes/examples/sweeps.yaml``

In examples/example_sweeps.yaml we provide example sweep file to test graph generation.

## 2. Database modification: 
The following fields are not provided automatically by hermes:
- group_by
- to_do
- due_date
- predecessors (i.e. parent experiments)

To enter predecessors manually one must provide a list of pairs 
``[[predecesor_name1, edge_label1], [predecesor_name2, edge_label2], ... ]``
It is important that predecessors are earlier in the database (sweep.yaml file) then their children.