import datetime
import json
from urllib import parse
import neptune
import collections

_database = []


# @TO: ignore this, it is an example
# of bad programming (though effective in this case)
def read_database(database):
    if database == "":
        # force reading experiments
        import experiments_graph.experiments_database
    else:
        with open(database) as f:
            exec(f.read())
    return _database


# @TO: this function might be buggy.
def create_neptune_url(neptune_org, neptune_project, tags, group_by):
    """
    Create a neptune link.

    :param neptune_org: organization in neptune e.g. pmtest
    :param neptune_project: project in neptune e.g. football
    :param tags: tags *warning* I am not sure if len(tags) != 2
    :param group_by: group by parameter # @TO, this is quite useful
        (you might not know it yet)
    :return: clickable url to neptune
    """
    if group_by is None:
        reference = parse.urlparse(
            'https://ui.neptune.ai/o/pmtest/org/football/experiments?viewId=standard-view&sortBy=%5B%22timeOfCreation%22%5D&sortDirection=%5B%22descending%22%5D&sortFieldType=%5B%22native%22%5D&trashed=false&tags=%5B%22naughty_blackwell%22%2C%22experiment_1813_cont2%22%5D&lbViewUnpacked=true')
    else:
        reference = parse.urlparse(
            "https://ui.neptune.ai/o/pmtest/org/sandbox/experiments?viewId=standard-view&sortBy=%5B%22timeOfCreation%22%5D&sortDirection=%5B%22descending%22%5D&sortFieldType=%5B%22native%22%5D&sortFieldAggregationMode=%5B%22auto%22%5D&trashed=false&suggestionsEnabled=true&tags=%5B%22baseline_pixel%22%5D&lbViewUnpacked=true"
        )

    new_path = rf'/o/{neptune_org}/org/{neptune_project}/experiments'
    query_dict = parse.parse_qs(reference.query)
    query_dict = {key: query_dict[key][0] for key in query_dict}

    # TODO: (do this properly)
    if len(tags) == 2:
        tags_str = json.dumps(tags[1:]).replace(" ", "")
    else:
        tags_str = json.dumps(tags).replace(" ", "")
    query_dict['tags'] = tags_str
    if group_by is not None:
        group_by_str = json.dumps([group_by[0]]).replace(" ", "")
        query_dict['groupBy'] = group_by_str

        if len(group_by) == 1:
            # Fallback for old format
            type = "numericParameters"
        else:
            type = group_by[1]

        group_by_type_str = json.dumps([type]).replace(" ", "")
        query_dict['groupByFieldType'] = group_by_type_str

    new_query_str = parse.urlencode(query_dict)

    new_url_data = list(reference)
    new_url_data[2] = new_path
    new_url_data[4] = new_query_str

    return parse.urlunparse(new_url_data)


class Sweep:
    def __init__(self,
                 name=None,
                 due_date=None,
                 neptune_org=None,
                 neptune_project=None,
                 neptune_tags=None,
                 note=None,
                 parent_experiments=None,
                 group_by=None,
                 to_do=False,
                 description=False,
                 style='standard'):
        """
        Sweep entry

        :param name: name of the experiment.
        :param due_date: time when the experiment should be reviewed.
            @TO: I found it quite useful when I run many experiments
            at the same time, to set this date as reminder.
        :param neptune_org:
        :param neptune_project:
        :param neptune_tags:
        :param note: note, description of the experiment.
        :param parent_experiments: experiments that will be linked to this one
            it should be list of (experiment, edge_str)
        :param group_by: see create_neptune_url
        :param to_do: a style marker # @TO see the class below
        :param description: a style marker # @TO see the class below
        """

        self.due_date = due_date
        self.group_by = group_by

        self.neptune_url = create_neptune_url(neptune_org, neptune_project,
                                              neptune_tags, group_by)
        self.neptune_org = neptune_org
        self.neptune_project = neptune_project
        self.neptune_tags = neptune_tags
        self.name = name
        self.parent_experiments = parent_experiments
        self.note = note or ''
        self.to_do = to_do
        self.description = description
        self.style = style

        # @TO: here is example how not to program. Ignore ;)
        _database.append(self)

    def set_parents(self, parent_experiments):
        self.parent_experiments = parent_experiments

    def __hash__(self):
        return abs(hash(self.neptune_url)) + abs(hash(self.name))

    def __eq__(self, other):
        return self.__hash__() == other.__hash__()

    def print_mermeid(self):

        review_str = ""
        if self.due_date is not None:
            review_str = rf'<br><br> Review at: {self.due_date.strftime("%d/%m/%Y, %H:%M")}'

        tags_str = ""
        for tag in self.neptune_tags:
            tags_str += tag + ", "
        tags_str = tags_str[:-2]

        note_str = self.note.replace("\n", "<br>")
        node_str = rf'{self.__hash__()}["{self.name}<br>{tags_str} {review_str}<br><small>{note_str}</small>"]'

        links_str = ""
        if self.parent_experiments is not None:
            for parent_experiment, edge_str in self.parent_experiments:
                comment_str = rf"|{edge_str}|" if edge_str is not None else ""
                links_str += rf'{parent_experiment.__hash__()}-->{comment_str}{self.__hash__()}' + '\n'


        if self.style is not None:
            class_ = self.style

        else:
            if self.to_do:
                class_ = "todo"
            elif self.description:
                class_ = "description"
            elif self.due_date is None:
                class_ = "done"
            elif self.due_date < datetime.datetime.now():
                class_ = "toReview"
            else:
                class_ = "standard"

        class_str = rf"class {self.__hash__()} {class_};"

        click_str = rf'click {self.__hash__()} "{self.neptune_url}"'

        return node_str + "\n" + links_str + "\n" \
               + class_str + "\n" + click_str+"\n"
