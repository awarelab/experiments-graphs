Here are ideas for mrunner. Some of these are probably not for now but it will be good to have them in mind.

* Probably the easiest will be to keep the experiment database in *yaml*. Yaml files tend to be heavy. Think how to mitigate that.
* Think how to make it usable. The operation, which users will need to do manually is defining links for the graph. This is a *sensitive* operation as it requires using id of parent experiments. It is essential that these are easy to find/infer from the graph.
* As above, this operation is error prone, think how to handle properly, if user misspecifies data/data format.   
* Add automatic inference of the cluster (eagle/prometheus) and do one button for logs.
* Think what should be displayed in the table. I'd be good to be able to filter/sort see e.g. [table](https://www.datatables.net/examples/data_sources/ajax.html)
* Users should be able to mark experiment (to have different colors on graphs)
* User might participate in many projects and thus use multiple graphs. Think how to define it.
* Make the graph, so it updates itself when new experiment appears or link is created.
