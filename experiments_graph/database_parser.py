
import yaml
from experiments_graph.experiments_graph_node import Sweep


def load_sweep_database(database_path):
    # Here we assume that parent must be always before children in the databse
    with open(database_path) as data_file:
        sweep_database = yaml.load(data_file, Loader=yaml.Loader)

    experiments_dict = dict()
    for name, params in sweep_database.items():
        note = params['note'] if 'note' in params else None
        new_experiment = Sweep(
            name=name, due_date=None,
            neptune_org=params['neptune_org'],
            neptune_project=params['neptune_project_name'],
            neptune_tags=params['neptune_tags'],
            note=note,
            group_by=params['group_by'],
            description=params['description'],
            style=params['style']
        )
        experiments_dict[name] = new_experiment

    for name, params in sweep_database.items():
        if params['predecessors'] is not None:
            parents = [(experiments_dict[p[0]], p[1]) for p in params['predecessors']]
            experiments_dict[name].set_parents(parents)

    return list(experiments_dict.values())
