import argparse
import os
from experiments_graph.database_parser import load_sweep_database
from experiments_graph.mermaid_renderer import generate_mermaid_html, \
    install_mermaid


# @TO: run with python experiments_graphs/experiments_graph/main_app.py /tmp/graph.html --database experiments_graphs/examples/example_database.py
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Create html file with '
                                                 'experiment graph.')
    parser.add_argument('file', type=str, help='path to dump html')
    parser.add_argument('--database', type=str, default="")
    parser.add_argument('--install_mermaid', type=bool, default=True,
                        help='install mermaid files')
    parser.add_argument('--dump_to_json', type=str, default=None)

    args = parser.parse_args()

    dir_path = os.path.dirname(args.file)
    os.makedirs(dir_path, exist_ok=True)

    # @TO; Little utility downloading mermaid
    if args.install_mermaid:
        install_mermaid(dir_path)

    # @PM: This is new way of reading the database from yaml of sweeps:
    experiments_database = load_sweep_database(args.database)

    # @PM: This remained unchanged
    # @TO: this is the most import part.
    # You probably want to use this, with some other
    # way of creating experiments_database
    generate_mermaid_html(experiments_database=experiments_database,
                          html_file_name=args.file)
