from jinja2 import Environment
import os

# @TO: styles of nodes, defines colors
styles = """

classDef standard fill:#FFDC00;
classDef done fill:#39CCCC;
classDef toReview fill:#FF851B;
classDef metaMissing fill:#f77;
classDef todo fill:#7FDBFF;
classDef description fill:#ffff00
"""

HTML = """
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Active experiments</title>
  <link rel="icon" type="image/png" href="data:image/png;base64,iVBORw0KGgo=">
</head>
<body>
    <div class="mermaid">
	  graph LR
		  {{experiments}}

		  {{styles}}
    </div>

  <script src="./mermaid.js"></script>
  <script>
    mermaid.initialize({
      theme: 'forest',
      // themeCSS: '.node rect { fill: red; }',
      logLevel: 1,
      flowchart: { curve: 'linear' },
      gantt: { axisFormat: '%m/%d/%Y' },
      sequence: { actorMargin: 50 },
      securityLevel:'loose'
	  // securityLevel='strict'
      // sequenceDiagram: { actorMargin: 300 } // deprecated
    });
  </script>
</body>
</html>
"""


# @TO, here is the most important point for you
def _get_mermaid_code(experiments_database):
    mermaid_code = ""
    for experiment in experiments_database:
        mermaid_code += experiment.print_mermeid()
    return mermaid_code


def generate_mermaid_html(experiments_database, html_file_name=None):
    mermaid_code = _get_mermaid_code(experiments_database)
    # @TO: jinja2 might be a little overkill here.
    html_str = Environment().from_string(HTML).render(experiments=mermaid_code,
                                                      styles=styles,
                                                      file_name=html_file_name)
    with open(html_file_name, "w") as f:
        f.write(html_str)


def install_mermaid(path):
    mermaid_file = os.path.join(path, "mermaid.js")
    if not os.path.isfile(mermaid_file):
        mermaid_install_command = rf"cd {path};curl https://registry.npmjs.org/mermaid/-/mermaid-8.5.0.tgz >mermaid.tgz;tar xf mermaid.tgz;cp package/dist/mermaid*js .;rm -rf package;rm mermaid.tgz"
        print("mermaid_install_command:", mermaid_install_command)
        os.system(mermaid_install_command)
